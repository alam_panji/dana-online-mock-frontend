

import React from 'react';
import {  Layout, Table, Row, Col, Button } from 'antd';

const { Content } = Layout
import Sidebar from '../components/layout/Sidebar'
import {
  injectIntl,
} from 'react-intl';

import {
  rolesService,
} from '../service';

const columns = [
  {
    title: 'Name',
    dataIndex: 'name',
  },
  {
    title: 'Age',
    dataIndex: 'age',
  },
  {
    title: 'Address',
    dataIndex: 'address',
  },
];

const data = [
  {
    key: '1',
    name: 'John Brown',
    age: 32,
    address: 'New York No. 1 Lake Park',
  },
  {
    key: '2',
    name: 'Jim Green',
    age: 42,
    address: 'London No. 1 Lake Park',
  },
  {
    key: '3',
    name: 'Joe Black',
    age: 32,
    address: 'Sidney No. 1 Lake Park',
  },
];

class Roles extends React.Component {
  state = {
    collapsed: false,
    listData: []
  };

  onCollapse = collapsed => {
    console.log(collapsed);
    this.setState({ collapsed });
  };

  async componentWillMount () {
    await this.fetchRoles();
  }

  fetchRoles = async () => {
    const res = await rolesService.getRolesList();
    this.setState({
      listData: res.data || [],
    });
  }

  render() {
    return (
      <div>
        <Sidebar>
          <Layout className="site-layout">
            <Content style={{ margin: '16px 16px' }}>
                <Row>
                  <Col span={12}>
                    <h2>Roles</h2>
                  </Col>
                  <Col span={4} offset = {8}>
                    <Button type="primary">Add New Roles</Button>
                  </Col>
                </Row>
                <Table columns={columns} dataSource={data} size="middle" />
            </Content>
          </Layout>
        </Sidebar>
      </div>
    );
  }
}

export default injectIntl(Roles);