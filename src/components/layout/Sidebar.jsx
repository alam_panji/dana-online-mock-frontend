import React from 'react';
import {  Layout, Menu, Breadcrumb  } from 'antd';

const { Header, Content, Footer, Sider } = Layout

import './sidebar.less';

export default ({children}) => {
    return (
      <Layout style={{ minHeight: '100vh' }}>
          <Sider collapsible>
          <div className="logo" />
          <Menu theme="dark" defaultSelectedKeys={['1']} mode="inline">
              <Menu.Item key="1">
              <span>Option 1</span>
              </Menu.Item>
              <Menu.Item key="2">
              <span>Option 2</span>
              </Menu.Item>
          </Menu>
          </Sider>
          { children }
      </Layout>
    );
};
