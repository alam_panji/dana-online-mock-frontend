import request from '../common/request';

export async function getRolesList () {
  return request('/api/roles', 'GET');
};
